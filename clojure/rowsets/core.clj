(ns rowsets.core
  (:use clojure.tools.trace)
  (:require  [clojure.set :as sets]))


(defn prnpass [n a]
  (prn n a)
  a
  )


(defn extract-row [erf parent root] 
  (if (vector? root)
    (map (fn [x] [(erf parent x) x]) root)
    [[(erf parent root) root]]))


(defn thread-vector [data f & args] 
  (if (seq? data)
    (map #(f % args) data)
    [(f data args)])) 


;; 
;; Maybe try a flattening approach
;; although with child parent we need to reverse things 
;;

(defn extract-trees [jdata parent extraction_tree] 
  (mapcat 
    (fn [row]
      (cons (first row)
            (mapcat  (fn [child_def]
                       (extract-trees  (get (last row) (first child_def)) (last (first row)) (last child_def)))
                    (:children extraction_tree)) )
      )
    (extract-row (:extf extraction_tree) parent jdata)))

;; An extraction is a record that takes an extractor function and a map of Extraction children
;; the keys of the children are used as a selector to choose the sub elements to apply the next
;; level of transormation to
;; the function extf
;; takes 
;; the parent object and the current selected json and returns
;; a pair - the fully qualified key for the row and the "row" itself
(defrecord Extraction [extf children])


(defn extractor 
  "Creates and extraction
    extf is a function
    (fn [parent cjson] ) -> [key row]
  
    cjson is the currently selected json object 
    parent is the parent object
  
    key is the fully qualified key of the object
   row is the row 

    fully qualified keys are:
     [:idea 1]
     [:human :us :joe :martin 12311414]


    the key is used for determining the action list stuff

    its important that it flatten everything
    "

  [extf children]
  (Extraction. extf children))

(defn build-action-lists 
  [list_a list_b]
  """Return sets of rows to operate on
    returns a vector of 3 vectors, the first is the list of items to add, second is items to delete
    and the third is the set to save for modification
    """

  (prn list_a list_b)

  (let [ma  (into {} list_a)
        mb (into {} list_b)
        ka (set (keys ma))
        kb (set (keys mb))
        toadd (sets/difference ka kb) 
        todel (sets/difference kb ka) 
        tocomp (sets/intersection ka kb) 
        delta (filter (fn [k] (not (= (get ma k) (get mb k)))) tocomp)
        ]
    [(map (partial get ma) toadd)
     (map (partial get mb) todel)
     (map (fn [k] [(get ma k) (get mb k)]) delta)] )) ;; deltas are a list if [new value, old value]



(defn map-action-lists [lists addf delf updf]
  (concat (map addf (nth lists 0)) (map delf (nth lists 1)) (map (fn [p] (updf (first p))) (nth lists 2))))
  



;; XXX recur
(defn walk [f root]
  (if (vector? root)
    (map #(walk f) root)
    (f root))) 








